*********************************************************************************
****************************************Readme***********************************
*********************************************************************************
El framework esta orquestado con composer y se utiliza el autoloader de composer.
Se respeta el PSR-4 para la aplicacion y el PSR-0 para el framework.

El framework presenta un Front Controller llamado index.php y su Bootstrap llamado
de la misma manera. Se tenia pensado crear un index_dev.php para cuando estemos en 
el modo desarrollo, y esto se le indicaba por el Bootstrap pero para el momento que 
escribo este Readme no tuve tiempo suficiente.

EL Routing fue usando un archivo llamado config.php

Mapeo URL-Acciones. Para ello se creo un archivo config.php,
alli se mapean las acciones y los controladores y se les asigna
un alias. Para cada controlador existe un alias y para cada combinacion de 
accion y controlador tambien. El framework no diferencia entre mayuscula y 
minuscula para que sea mas agradable para el usuario. En caso de que una 
accion o un controlador no exista, se carga el controlador y la accion por 
defecto (BasicController y indexAction). Lo mismo sucede cuando no se colocan
controladores ni actions en la url. Las opciones de controladores disponibles en
config son 'Other', 'Basic', 'Error'.

Existe un Controller tipo (Abstracto) dentro del Framework, del cual extienden todos 
los controladores (Dentro de la aplicacion). Se crearon los objetos Request y Response.

Cuando se cargan algunos controladores y/o acciones predeterminadas, encontrara
un ejemplo del template de Smarty funcionando correctamente. Twig fue instalado 
con composer tambien y se creo su clase Twig (Twig y Smarty extienden de una clase
que llamamos template), pero no pude realizar un ejemplo del mismo en el framework.

Se creo el archivo DATABASE pero no me dio tiempo de configurarlo correctamente. No
logre culminar el container aunque entiendo habria que hacer otro archivo config con
las Keys y los nombres de los servicios que se necesitaran, como se menciono en clase.


